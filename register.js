Router.route('/register');

if (Meteor.isClient) {
    Template.register.helpers({
        tlocations: function() {
            return Locations.find({}, {sort: {description: 1}});
        }
    });

    Template.register.events({
        'submit form': function(event) {
            event.preventDefault();

            Accounts.createUser({
                email: event.target.registerEmail.value,
                password: event.target.registerPassword.value,
                profile: {location: event.target.registerLocation.value}
            });

            Router.go('home');
        }
    });
}
