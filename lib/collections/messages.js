Messages = new Mongo.Collection('messages');

if (Meteor.isServer) {
    Meteor.publish('messages', function() {
        if (this.userId) {
            var userLocation = Meteor.users.findOne({'_id': this.userId}).profile.location;
            return Messages.find({'location': userLocation}, {sort: {createdAt: -1}});
        } else {
            this.ready();
        }
    });
}

if (Meteor.isClient) {
    Meteor.subscribe('messages');

    var descriptionOfLocation = function() {
        return Locations.findOne({location: Meteor.user().profile.location}).description;
    };

    Template.messageList.helpers({
        tmessages: function() {
            return Messages.find().fetch();
        },
        currentLocation: descriptionOfLocation,
        isOwner: function() {
            return this.owner === Meteor.userId();
        }
    });

    Template.messageList.events({
        'submit .new-message': function(event) {
            event.preventDefault();
            Meteor.call('addMessage', event.target.text.value);
            event.target.text.value = '';
        },
        'click .delete-message': function() {
            Meteor.call('deleteMessage', this._id);
        },
        'click .edit-message': function(event) {
            event.preventDefault();
        }
    });

    Template.messageEdit.helpers({
        currentLocation: descriptionOfLocation
    });

    Template.messageEdit.events({});
}

Meteor.methods({
    addMessage: function(text) {
        // Make sure the user is logged in before inserting a message
        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }

        Messages.insert({
            owner: Meteor.userId(),
            createdAt: new Date(),
            location: Meteor.user().profile.location,
            status: MESSAGE_NEW,
            message: text
        });
    },

    deleteMessage: function(messageId) {
        var message = Messages.findOne(messageId);
        if (message.owner == Meteor.userId()) {
            Messages.remove(messageId);
        }
    }
});

// schema
Messages.attachSchema(new SimpleSchema({
    // Owner
    owner: {
        type: String,
        label: 'Owner',
        autoValue: function() {
            return Meteor.userId();
        },
        optional: false
    },
    // where from
    location: {
        type: String,
        label: 'For whom',
        autoValue: function() {
            return Meteor.user().profile.location;
        },
        allowedValues: function() {
            Locations.find({}, {sort: {description: 1}}).map(function() {
                return this.location;
            });
        },
        min: 2,
        max: 2
    },
    // Status (statuses.js)
    status: {
        type: Number,
        label: 'Status of the message',
        autoValue: function() {
            return this.isInsert ? MESSAGE_NEW : MESSAGE_EDITED;
        }
    },
    // Created At
    createdAt: {
        type: Date,
        autoValue: function() {
            if (this.isInsert) {
                return new Date();
            } else if (this.isUpdate) {
                return {
                    $setOnInsert: new Date()
                };
            } else {
                this.unset();
            }
        },
        denyUpdate: true
    },
    // Modified At
    updatedAt: {
        type: Date,
        autoValue: function() {
            if (this.isUpdate) {
                return new Date();
            }
        },
        denyInsert: true,
        optional: true
    },
    // text of the message
    message: {
        type: String,
        max: 1000,
        optional: true,
        trim: true
    }
}));
