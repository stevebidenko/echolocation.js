Locations = new Mongo.Collection('locations');

if (Meteor.isServer) {
    Meteor.publish('locations', function() {
        return Locations.find();
    });
}

if (Meteor.isClient) {
    Meteor.subscribe('locations');
}

Locations.attachSchema(new SimpleSchema({
    // Name of Location
    location: {
        type: String,
        label: 'Location',
        min: 2,
        max: 2
    },
    description: {
        type: String,
        label: 'Description',
        min: 2,
        max: 16
    }
}));

