Router.route('/password');

if (Meteor.isClient) {
    Template.password.helpers({
        email: function() {
            return Meteor.user().emails[0].address;
        }
    });

    Template.password.events({
        'submit form': function(event) {
            var target = event.target,
                isNewPassword = target.newPassword.value !== "" && target.newPassword.value === target.repPassword.value;
            event.preventDefault();
            if (isNewPassword) {
                Accounts.changePassword(target.oldPassword.value, target.newPassword.value);
            }
        }
    });
}
