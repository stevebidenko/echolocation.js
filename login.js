Router.route('/login');

if (Meteor.isClient) {
    Template.login.events({
        'submit form': function(event) {
            event.preventDefault();
            var email = event.target.loginEmail.value;
            var pwd = event.target.loginPassword.value;
            Meteor.loginWithPassword(email, pwd, function(err) {
                if (err) {
                    alert(err);
                }
                Router.go('home');
            });
        }
    });
}
