**Initial Requirements**

Create a messaging web-app on meteor.js with the following functionality:

- User can register via email/password
- User can sign in and edit his profile's email, password, name and select location from predefined-list
- User can broadcast message and all users within his current location will see it
- In case user changes his location his old messages will remain visible to previous location and won't move to a new one

An app should be pushed to a git-repo and deployed somewhere (possibly to free meteor.com dev hosting). It's not necessary that app has awesome design, but it should be usable.

**The solution**

1. Add to MongoDB using by the command 'meteor mongo' the following records:
```javascript
db.locations.insert([
  {_id: 1, location: "BC", description: "Lviv"},
  {_id: 2, location: "AI", description: "Kiev"},
  {_id: 3, location: "AX", description: "Kharkiv"},
  {_id: 4, location: "BH", description: "Odessa"},
  {_id: 5, location: "AE", description: "Dnepropetrovsk"}
]);
```
2. Launch this application using by the following command:
```
$ meteor
```
3. Open the site http://localhost:3000
