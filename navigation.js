if (Meteor.isClient) {
    Template.navigation.events({
        'click .logout': function (event) {
            event.preventDefault();
            Meteor.logout(function () {
                Router.go('home');
            });
        }
    });

    Template.navigation.helpers({
        currentEmail: function () {
            return Meteor.user().emails[0].address;
        },
        currentName: function () {
            return Meteor.user().profile.name;
        }
    });
}
