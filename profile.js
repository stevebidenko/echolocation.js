Router.route('/profile');

if (Meteor.isClient) {
    Template.registerHelper('selectedIfEq', function(value, current) {
        return value == current ? 'selected' : '';
    });

    Template.profile.helpers({
        tlocations: function() {
            return Locations.find({}, {sort: {description: 1}});
        },
        userLocation: function() {
            return Meteor.user().profile.location;
        },
        fullName: function() {
            return Meteor.user().profile.name;
        },
        email: function() {
            return Meteor.user().emails[0].address;
        }
    });

    Template.profile.events({
        'submit form': function(event) {
            var target = event.target;

            event.preventDefault();
            Meteor.call('updateProfile', target.editEmail.value, {
                location: target.editLocation.value,
                name: target.fullName.value
            });
            Router.go('home');
        }
    });

}

Meteor.methods({
    updateProfile: function(newEmail, newProfile) {
        if (this.userId) {
            Meteor.users.update(this.userId, {
                $set: {
                    emails: [{address: newEmail, verified: false}],
                    profile: newProfile
                }
            });
        }
    }
});

if (Meteor.isServer) {
}
